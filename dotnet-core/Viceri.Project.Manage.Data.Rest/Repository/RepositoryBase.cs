﻿using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Viceri.Project.Manager.Data.Rest.Model;
using Viceri.Project.Manager.Domain.Config;

namespace Viceri.Project.Manager.Data.Rest.Repository
{
    public class RepositoryBase : RestClient
    {
        private string _url;
        private string _privateToken;

        public RepositoryBase(IOptions<ManagerSettings> settings)
            : base()
        {
            var url = settings.Value.GitLabUrl;
            var privateToken = settings.Value.GitLabPrivateToken;

            if (string.IsNullOrEmpty(url))
                throw new Exception("Base URL da API nao configurada");

            if (string.IsNullOrEmpty(privateToken))
                throw new Exception("Base URL da API nao configurada");

            _url = url;
            _privateToken = privateToken; 

            BaseUrl = new Uri(url);
        }

        private void AddAuthorizationToHeader(IRestRequest requisicao)
        {
            requisicao.AddHeader("PRIVATE_TOKEN", _privateToken);
        }

        private IRestResponse ExecuteOrThrowException(IRestRequest request)
        {
            AddAuthorizationToHeader(request);

            var response = Execute(request);

            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new Exception(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new ApplicationException("Error retrieving response. Check inner details for more info.", response.ErrorException);

            return response;
        }

        private IRestResponse<T> ExecuteOrThrowException<T>(IRestRequest request)
            where T : new()
        {
            AddAuthorizationToHeader(request);

            var response = Execute<T>(request);

            return response;
        }

        public IRestResponse<T> Post<T>(string resource, object model) where T : new()
        {
            var requisicao = new RestRequest(resource, Method.POST);
            requisicao.AddJsonBody(model);

            return ExecuteOrThrowException<T>(requisicao);
        }

        public string Get(string resource, params QueryStringParameter[] parameters)
        {
            var requisicao = new RestRequest(resource, Method.GET);

            foreach (var parameter in parameters)
                requisicao.AddQueryParameter(parameter.Name, parameter.Value);

            var resposta = ExecuteOrThrowException(requisicao);

            if (resposta.Content == "null")
                return null;

            return resposta.Content;
        }

        public async Task<List<T>> GetAsync<T>(string resource, params QueryStringParameter[] parameters)
            where T : new()
        {
            var requisicao = new RestRequest(resource, Method.GET);

            foreach (var parameter in parameters)
                requisicao.AddQueryParameter(parameter.Name, parameter.Value);

            var resposta = ExecuteOrThrowException<List<T>>(requisicao);
            return resposta.Data;
        }

        public async Task<T> GetFirstOrDefault<T>(string resource, params QueryStringParameter[] parameters)
            where T : class, new()
        {
            var result = await GetAsync<T>(resource, parameters);
            return result == null ? null : result.FirstOrDefault();
        }
    }
}
