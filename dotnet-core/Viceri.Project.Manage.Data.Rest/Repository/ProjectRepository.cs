﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Viceri.Project.Manager.Domain.Config;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Model;

namespace Viceri.Project.Manager.Data.Rest.Repository
{
    public class ProjectRestRepository : RepositoryBase, IProjectRestRepository
    {
        IOptions<ManagerSettings> _settings;

        public ProjectRestRepository(IOptions<ManagerSettings> settings) : base(settings)
        {
            _settings = settings;
        }

        public async Task<ICollection<ProjectRest>> ListAllAsync()
        {
            var models = await GetAsync<ProjectRest>("/projects");

            if (models != null)
                return models
                    .Where(w => w.ForksCount == 0)
                    .ToList();
            else
                return null;
        }
    }
}
