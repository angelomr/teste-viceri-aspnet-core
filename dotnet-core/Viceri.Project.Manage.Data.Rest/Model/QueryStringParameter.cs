﻿using System.Globalization;

namespace Viceri.Project.Manager.Data.Rest.Model
{
    public class QueryStringParameter
    {
        public QueryStringParameter()
        {
        }

        public QueryStringParameter(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public QueryStringParameter(string name, int value)
        {
            Name = name;
            Value = value.ToString();
        }

        public QueryStringParameter(string name, int? value)
        {
            Name = name;
            Value = value.HasValue ? value.Value.ToString() : null;
        }

        public QueryStringParameter(string name, decimal value)
        {
            Name = name;
            Value = value.ToString(CultureInfo.GetCultureInfo("en-US"));
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}
