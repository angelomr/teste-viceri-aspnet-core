﻿namespace Viceri.Project.Manager.Domain.Config
{
    public class ManagerSettings
    {
        public string GitLabUrl { get; set; }
        public string GitLabPrivateToken { get; set; }
        public string GitLabUser { get; set; }
    }
}
