﻿using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IUserRepository
    {
        Task<User> Login(string username, string password);
    }
}
