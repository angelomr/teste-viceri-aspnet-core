﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectService
    {
        Task<ICollection<Entity.Project>> ListAllProjectsAsync();
        Task SaveIssue(Entity.Issue issue);
        Task SaveList(List<Entity.Project> listProject);
    }
}
