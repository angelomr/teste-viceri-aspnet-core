﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Model;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectRestRepository
    {
        Task<ICollection<ProjectRest>> ListAllAsync();
    }
}
