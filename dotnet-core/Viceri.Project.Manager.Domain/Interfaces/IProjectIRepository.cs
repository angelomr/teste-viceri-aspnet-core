﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectIRepository
    {
        Task<ICollection<Entity.Project>> ListAllAsync();
        Task SaveList(List<Entity.Project> listProject);
    }
}
