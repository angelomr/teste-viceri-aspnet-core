﻿using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IUserService
    {
        Task<User> Login(string username, string password);
    }
}
