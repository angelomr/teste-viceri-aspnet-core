﻿using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IIssueRepository
    {
        Task Save(Issue issue);
    }
}
