﻿using System;

namespace Viceri.Project.Manager.Domain.Entities
{
    public class Project
    {
        public Guid Id { get; set; }
        public int IdProject { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string WebUrl { get; set; }
    }
}
