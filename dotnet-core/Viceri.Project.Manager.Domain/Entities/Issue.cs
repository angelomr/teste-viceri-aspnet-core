﻿using System;

namespace Viceri.Project.Manager.Domain.Entities
{
    public class Issue
    {
        public Guid Id { get; set; }
        public int IdIssue { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateAt { get; set; }
        public string State { get; set; }
    }
}
