﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Entity = Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectIRepository _projectRepository;
        private readonly IIssueRepository _issueRepository;
        private readonly IProjectRestRepository _projectRestRepository;

        public ProjectService(IProjectIRepository projectRepository, IProjectRestRepository projectRestRepository, IIssueRepository issueRepository)
        {
            _projectRepository = projectRepository;
            _projectRestRepository = projectRestRepository;
            _issueRepository = issueRepository;
        }
        
        public async Task<ICollection<Entity.Project>> ListAllProjectsAsync()
        {
            return await _projectRepository.ListAllAsync();
        }

        public async Task SaveIssue(Entity.Issue issue)
        {
            await _issueRepository.Save(issue);
        }

        public async Task SaveList(List<Entity.Project> listProject)
        {
            await _projectRepository.SaveList(listProject);
        }
    }
}
