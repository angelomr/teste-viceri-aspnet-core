﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Model;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ProjectRestService : IProjectRestService
    {
        private readonly IProjectRestRepository _projectRestRepository;

        public ProjectRestService(IProjectRestRepository projectRestRepository)
        {
            _projectRestRepository = projectRestRepository;
        }

        public async Task<ICollection<ProjectRest>> ListAllAsync()
        {
            return await _projectRestRepository.ListAllAsync();
        }
    }
}
