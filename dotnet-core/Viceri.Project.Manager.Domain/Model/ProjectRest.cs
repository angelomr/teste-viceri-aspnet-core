﻿using Newtonsoft.Json;

namespace Viceri.Project.Manager.Domain.Model
{
    public class ProjectRest
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string WebUrl { get; set; }
        [JsonProperty("forks_count")]
        public int ForksCount { get; set; }
    }
}
