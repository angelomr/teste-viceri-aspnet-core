﻿using Newtonsoft.Json;
using System;

namespace Viceri.Project.Manager.Domain.Model
{
    public class IssueEvent
    {
        [JsonProperty("object_attributes")]
        public Issue Issue { get; set; }
    }
    
    public class Issue
    {
        [JsonProperty("id")]
        public int IdIssue { get; set; }
        public string Description { get; set; }
        public string DueDate { get; set; }
        public string Title { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreateAt { get; set; }
        public string State { get; set; }
    }
}
