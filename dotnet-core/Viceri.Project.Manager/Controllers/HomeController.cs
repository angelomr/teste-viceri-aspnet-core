﻿using Microsoft.AspNetCore.Mvc;

namespace Viceri.Project.Manager.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }
    }
}