﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application;

namespace Viceri.Project.Manager.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly ViceriProjectManagerSettings _settings;
        private readonly IUserApplication _userApplication;

        public TokenController(IUserApplication userApplication, ViceriProjectManagerSettings settings)
        {
            _userApplication = userApplication;
            _settings = settings;

        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] TokenRequest tokenRequest)
        {
            if (tokenRequest == null)
                return BadRequest();

            var user = await _userApplication.Login(tokenRequest.Username, tokenRequest.Password);
            if (user != null)
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, tokenRequest.Username)
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.IssuerKey));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(
                    issuer: _settings.Issuer,
                    audience: _settings.TokenAudirence,
                    claims: claims,
                    expires: DateTime.UtcNow.AddSeconds(_settings.TokenExp),
                    signingCredentials: creds);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
            else
                return BadRequest("Invalid username or password.");
        }
    }

    public class TokenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
