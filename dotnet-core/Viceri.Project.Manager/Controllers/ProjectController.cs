﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Domain.Model;

namespace Viceri.Project.Manager.Controllers
{
    [Produces("application/json")]
    [Route("api/project")]
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectApplication _projectApplication;

        public ProjectController(IProjectApplication projectApplication)
        {
            _projectApplication = projectApplication;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var list = await _projectApplication.ListAllProjectsAsync();

            if (list != null)
                return Ok(list);
            else
                return BadRequest("The repository is empty.");
        }

        [HttpPost]
        [Route("issue")]
        public async Task<IActionResult> PostEvent([FromBody]IssueEvent issue)
        {
            await _projectApplication.SaveIssue(issue);
            return Ok();
        }
    }
}