﻿using Viceri.Project.Manager.Domain.Model;

namespace Viceri.Project.Manager.Application.Mappings
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<ProjectRest, Domain.Entities.Project>()
                .ForMember(dest => dest.IdProject, opt => opt.MapFrom(src => src.Id));

            CreateMap<Issue, Domain.Entities.Issue>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreateAt, opt => opt.MapFrom(src => src.CreateAt))
                .ForMember(dest => dest.IdIssue, opt => opt.MapFrom(src => src.IdIssue))                
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            CreateMap<Domain.Entities.User, ViewModels.UserViewModel>();
        }
    }
}
