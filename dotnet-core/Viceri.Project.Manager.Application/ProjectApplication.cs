﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Model;
using Entity = Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Application.Extensions;
using System.Linq;

namespace Viceri.Project.Manager.Application
{
    public class ProjectApplication : IProjectApplication
    {
        private readonly IProjectService _projectService;
        private readonly IProjectRestService _projectRestService;

        public ProjectApplication(IProjectService projectService, IProjectRestService projectRestService)
        {
            _projectService = projectService;
            _projectRestService = projectRestService;
        }

        public async Task<ICollection<Entity.Project>> ListAllProjectsAsync()
        {
            ICollection<Entity.Project> projectReturn = new List<Entity.Project>();

            // busca projetos da base de dados
            var projectDB = await _projectService.ListAllProjectsAsync();
            // busca projetos do gitlab
            var projectGitLab = await _projectRestService.ListAllAsync();

            // gera lista de projetos a serem salvos, que não foram importados
            var listaIgnora = projectGitLab.Where(w => projectDB.Select(s => s.IdProject).ToList().Contains(w.Id));
            var listSalvar = projectGitLab.Except(listaIgnora);

            // salva novos projetos e retorna lista atualizada
            await _projectService.SaveList(listSalvar.MapTo<List<Entity.Project>>());
            var listaAtualizada = await _projectService.ListAllProjectsAsync();

            return listaAtualizada;
        }

        public async Task SaveIssue(IssueEvent issue)
        {
            var dadosIssue = issue.Issue;
            await _projectService.SaveIssue(dadosIssue.MapTo<Entity.Issue>());
        }
    }
}
