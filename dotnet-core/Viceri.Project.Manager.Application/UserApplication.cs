﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Extensions;
using Viceri.Project.Manager.Application.ViewModels;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Application
{
    public class UserApplication : IUserApplication
    {
        private readonly IUserService _userService;

        public UserApplication(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<UserViewModel> Login(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            var user = await _userService.Login(username, CreateHash(password));
            return user.MapTo<UserViewModel>();
        }

        private static string CreateHash(string password)
        {

            using (var algorithm = SHA256.Create())
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(password));
                var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                return hash;
            }
        }
    }
}
