﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Model;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Application
{
    public interface IProjectApplication
    {
        Task<ICollection<Entity.Project>> ListAllProjectsAsync();
        Task SaveIssue(IssueEvent issue);
    }
}