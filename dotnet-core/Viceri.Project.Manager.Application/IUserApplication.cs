﻿using System.Threading.Tasks;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Application
{
    public interface IUserApplication
    {

        Task<UserViewModel> Login(string username, string password);
    }
}
