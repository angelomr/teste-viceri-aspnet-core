﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Security.Cryptography;
using System.Text;
using Viceri.Project.Manager.Data.EntityTypeConfigurations;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data
{
    public class ViceriProjectManagerContext : DbContext
    {
        public DbSet<Entity.Project> Projects { get; set; }
        public DbSet<Entity.Issue> Issues { get; set; }
        public DbSet<Entity.User> Users { get; set; }

        public ViceriProjectManagerContext(DbContextOptions<ViceriProjectManagerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProjectTypeConfiguration());
            modelBuilder.ApplyConfiguration(new IssueTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());

            modelBuilder.Entity<Entity.User>()
                .HasData(new Entity.User {
                    Id = 1,
                    Username = "admin",
                    Password = CreateHash("123456"),
                    Email = "admin@admin.com",
                    IsDeleted = false
                });
        }

        private static string CreateHash(string password)
        {

            using (var algorithm = SHA256.Create())
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(password));
                var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                return hash;
            }
        }
    }

    public class PrecoContextDesignFactory : IDesignTimeDbContextFactory<ViceriProjectManagerContext>
    {
        public ViceriProjectManagerContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ViceriProjectManagerContext>()
                .UseSqlServer("Server=localhost,1433;Initial Catalog=Viceri;Persist Security Info=False;User ID=sa;Password=@123ABCd#;Connection Timeout=30;");

            return new ViceriProjectManagerContext(optionsBuilder.Options);
        }        
    }
}
