﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class IssueTypeConfiguration : IEntityTypeConfiguration<Entity.Issue>
    {
        public void Configure(EntityTypeBuilder<Entity.Issue> builder)
        {
            builder.ToTable("Issue");

            builder.HasKey(k => k.Id);
        }
    }
}
