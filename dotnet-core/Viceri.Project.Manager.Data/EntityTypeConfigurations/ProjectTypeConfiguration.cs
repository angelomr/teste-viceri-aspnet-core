﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class ProjectTypeConfiguration : IEntityTypeConfiguration<Entity.Project>
    {
        public void Configure(EntityTypeBuilder<Entity.Project> builder)
        {
            builder.ToTable("Project");

            builder.HasKey(k => k.Id);
        }
    }
}
