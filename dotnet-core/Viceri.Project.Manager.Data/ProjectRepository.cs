﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entity = Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class ProjectRepository : IProjectIRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public ProjectRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Entity.Project>> ListAllAsync()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task SaveList(List<Entity.Project> listProject)
        {
            foreach(var project in listProject)
            {
                project.Id = Guid.NewGuid();
                await _context.Projects.AddAsync(project);
            }

            _context.SaveChanges();
        }
    }
}
