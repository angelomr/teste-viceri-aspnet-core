﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Entity = Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class IssueRepository : IIssueRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public IssueRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public async Task Save(Entity.Issue issue)
        {
            var issueTmp = await _context.Issues.FirstOrDefaultAsync(w => w.IdIssue == issue.IdIssue);

            if (issueTmp != null)
            {
                issueTmp.Description = issue.Description;
                issueTmp.State = issue.State;
                issueTmp.Title = issue.Title;

                _context.Issues.Update(issueTmp);
            }
            else
            {
                await _context.Issues.AddAsync(issue);
            }

            await _context.SaveChangesAsync();
        }
    }
}
