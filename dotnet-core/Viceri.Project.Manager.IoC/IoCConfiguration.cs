﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Data;
using Microsoft.Extensions.Configuration;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Services;
using Viceri.Project.Manager.Data.Rest.Repository;

namespace Viceri.Project.Manager.IoC
{
    public static class IoCConfiguration
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            ConfigureDomain(services);
            ConfigureData(services, configuration);
            ConfigureApplication(services);
        }

        private static void ConfigureApplication(IServiceCollection services)
        {
            services.AddScoped<IProjectApplication, ProjectApplication>();
            services.AddScoped<IUserApplication, UserApplication>();
        }

        private static void ConfigureData(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ViceriProjectManagerContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddScoped<IProjectRestRepository, ProjectRestRepository>();
            services.AddScoped<IProjectIRepository, ProjectRepository>();
            services.AddScoped<IIssueRepository, IssueRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }

        private static void ConfigureDomain(IServiceCollection services)
        {
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IProjectRestService, ProjectRestService>();
            services.AddScoped<IUserService, UserService>();
        }
    }
}
