﻿using Moq;
using System.Collections.Generic;
using System.Linq;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Model;
using Xunit;
using Entity = Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Tests.Application
{
    public class ProjectApplicationiTest : UnitTestBase
    {
        private readonly Mock<IProjectService> _projectServiceMock;
        private readonly Mock<IProjectRestService> _projectRestServiceMock;

        public ProjectApplicationiTest()
        {
            _projectServiceMock = new Mock<IProjectService>();
            _projectRestServiceMock = new Mock<IProjectRestService>();
        }

        [Fact]
        public void List_All_Project_Success()
        {
            ICollection<ProjectRest> listMock = new List<ProjectRest>();
            listMock.Add(new ProjectRest
            {
                Id = 1,
                Description = "Project One",
                Name = "Name Project One",
                WebUrl = "https://gitlab.com/projectone"
            });
            listMock.Add(new ProjectRest
            {
                Id = 2,
                Description = "Project Two",
                Name = "Name Project Two",
                WebUrl = "https://gitlab.com/projecttwo"
            });

            ICollection<Entity.Project> listProjMock = new List<Entity.Project>();
            listProjMock.Add(new Entity.Project
            {
                IdProject = 1,
                Description = "Project One",
                Name = "Name Project One",
                WebUrl = "https://gitlab.com/projectone"
            });
            listProjMock.Add(new Entity.Project
            {
                IdProject = 2,
                Description = "Project Two",
                Name = "Name Project Two",
                WebUrl = "https://gitlab.com/projecttwo"
            });

            _projectServiceMock.Setup(s => s.ListAllProjectsAsync())
                .ReturnsAsync(listProjMock);
            _projectRestServiceMock.Setup(s => s.ListAllAsync())
                .ReturnsAsync(listMock);

            var projectApplication = new ProjectApplication(_projectServiceMock.Object, _projectRestServiceMock.Object);
            var listProject = projectApplication.ListAllProjectsAsync().GetAwaiter().GetResult();

            Assert.True(listProject.Count == 2);
            Assert.True(listProject.Where(l => l.IdProject == 2) != null);
        }
    }
}
