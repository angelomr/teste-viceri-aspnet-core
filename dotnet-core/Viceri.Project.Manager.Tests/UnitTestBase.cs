﻿namespace Viceri.Project.Manager.Tests
{
    public abstract class UnitTestBase
    {
        private volatile static bool autoMapperInitialized = false;
        private static object lockObject = new object();

        public UnitTestBase()
        {
            InitializeAutomapper();
        }

        private static void InitializeAutomapper()
        {
            lock (lockObject)
            {
                if (!autoMapperInitialized)
                {
                    AutoMapper.Mapper.Initialize((cfg) =>
                    {
                        cfg.AddProfile<Manager.Application.Mappings.MappingProfile>();
                    });
                    autoMapperInitialized = true;
                }
            }
        }
    }
}
