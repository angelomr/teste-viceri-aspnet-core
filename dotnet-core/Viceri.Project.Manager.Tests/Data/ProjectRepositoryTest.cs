﻿using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Data.Rest.Repository;
using Viceri.Project.Manager.Domain.Config;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Domain.Model;
using Xunit;

namespace Viceri.Project.Manager.Tests.Application
{
    public class ProjectRepositoryTest
    {
        public ProjectRepositoryTest()
        {            
        }

        [Fact]
        public void List_All_Project_Success()
        {
            ICollection<ProjectRest> listMock = new List<ProjectRest>();
            listMock.Add(new ProjectRest
            {
                Id = 1,
                Description = "Project One",
                Name = "Name Project One",
                WebUrl = "https://gitlab.com/projectone"
            });
            listMock.Add(new ProjectRest
            {
                Id = 2,
                Description = "Project Two",
                Name = "Name Project Two",
                WebUrl = "https://gitlab.com/projecttwo"
            });

            var _projectRepository = new Mock<IProjectRestRepository>();
            _projectRepository.Setup(s => s.ListAllAsync())
                .ReturnsAsync(listMock);

            var listProject = _projectRepository.Object.ListAllAsync().GetAwaiter().GetResult();

            Assert.True(listProject.Count == 2);
            Assert.True(listProject.Where(l => l.Id == 2) != null);
        }
    }
}
