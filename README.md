# Projeto Teste Viceri

1 - O projeto utiliza um banco de dados local SQL Express utilizando a seguinte string de conexão:
Server=localhost,1433;Initial Catalog=Viceri;Persist Security Info=False;User ID=sa;Password=@123ABCd#;Connection Timeout=30;

2 - Utiliza seguinte url do IIS Express:
http://localhost:53662

3 - O arquivo Teste.postman_collection.json contém os endpoints de teste, para serem utilizados no postman.

4 - Foi desenvolvido o endpoint para receber o webhooks de Issues.

5 - Não foi possível receber os eventos do webhooks, devido a necessidade de uma url externa (pública) para recebê-los.